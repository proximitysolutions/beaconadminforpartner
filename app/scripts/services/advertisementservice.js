/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


'use strict';
angular.module('myApp')
	.factory('AdvertisementService', ['$resource', 'ServiceConfig', 'SessionService',
	    function ($resource, ServiceConfig, SessionService) {

		var apiUrl = ServiceConfig.getUrl();
		var sessionId = SessionService.getSessionId();
		return $resource(null,
			{
			    sessionId: sessionId
			},
		{
		    addAdvertisement: {
			method: 'GET',
			url: apiUrl + '?method=beacon_advertise_addads'
		    },editAdvertisement: {
			method: 'GET',
			url: apiUrl + '?method=partner_advertise_edit'
		    },
		    getAdvertisement: {
			method: 'GET',
			url: apiUrl + '?method=beacon_advertise_getads'
		    },
		    updateAdvertisement: {
			method: 'GET',
			url: apiUrl + '?method=beacon_advertise_updateads'
		    },
		    removeAdvertisement: {
			method: 'GET',
			url: apiUrl + '?method=partner_remove_advertisement'
		    },
		    addAdvertisementsToBeacon: {
			method: 'GET',
			url: apiUrl + '?method=beacon_advertise_addadstobeacon'
		    },
		    removeAdvertisementsFromBeacon: {
			method: 'GET',
			url: apiUrl + '?method=beacon_advertise_removeadsfrombeacon'
		    },
		    listAdvertisement: {
			method: 'GET',
			url: apiUrl + '?method=partner_getadlist'
		    },
		    listAdvertisementFromBeacon: {
			method: 'GET',
			url: apiUrl + '?method=beacon_advertise.getlistfrombeacon'
		    }

		});
	    }]);
'use strict';
/**
 * @ngdoc service
 * @name myAppApp.UserService
 * @description
 * # UserService
 * Factory in the myAppApp.
 */
angular.module('myApp')
	.factory('UserService', ['$resource', 'ServiceConfig', 'SessionService',
	    function ($resource, ServiceConfig, SessionService) {
		var apiUrl = ServiceConfig.getUrl();
		var sessionId = SessionService.getSessionId();

		return $resource(null, {
		    sessionId: sessionId
		}, {
		    logIn: {
			method: 'GET',
			url: apiUrl + '?method=partner_login'
		    },
		    addUser: {
			method: 'GET',
			url: apiUrl + '?user=:user&password=:password&method=user.add'
		    },
		    getUser: {
			method: 'GET',
			url: apiUrl + '?method=user.get'
		    },
		    listUser: {
			method: 'GET',
			url: apiUrl + '?method=user.list'
		    },
		    getUserBySession: {
			method: 'GET',
			url: apiUrl + '?method=user.getBySession'
		    }
		});
	    }]);
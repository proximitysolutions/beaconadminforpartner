/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


'use strict';
angular.module('myApp')
	.factory('PartnerService', ['$resource', 'ServiceConfig', 'SessionService',
	    function ($resource, ServiceConfig, SessionService) {

		var apiUrl = ServiceConfig.getUrl();
		var sessionId = SessionService.getSessionId();
		return $resource(null,
			{
			    sessionId: sessionId
			},
		{
		    addPartner: {
			method: 'GET',
			url: apiUrl + '?method=beacon_partner.addpartner'
		    },
		    getPartner: {
			method: 'GET',
			url: apiUrl + '?partnerId=:partnerId&method=beacon_partner.getpartner'
		    },
		    updatePartner: {
			method: 'GET',
			url: apiUrl + '?partnerId=:partnerId&name=:name&address=:address&enable=:enable&method=beacon_partner.updatepartner'
		    },
		    removePartner: {
			method: 'GET',
			url: apiUrl + '?partnerId=:partnerId&method=beacon_partner.remove'
		    },
		    listPartner: {
			method: 'GET',
			url: apiUrl + '?&method=beacon_partner.getlist'
		    },
		    listBeacon: {
			method: 'GET',
			url: apiUrl + '?&method=beacon_partner.getbeacons'
		    },
		    addBeacons: {
			method: 'GET',
			url: apiUrl + '?&method=beacon_partner.addbeacons'
		    }

		});
	    }]);
'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
    'ngRoute',
    'ngResource',
    'ngCookies',
    'ui.bootstrap',
    'smart-table'
]).
        config(['$routeProvider', function ($routeProvider) {
                $routeProvider
                .when('/home', {
			templateUrl: 'views/beaconlist.html',
			controller: 'BeaconListCtrl',
			activetab: 'home'
		    })
                    .when('/advertisements', {
			templateUrl: 'views/advertisementlist.html',
			controller: 'AdvertisementListCtrl',
			activetab: 'advertisements'
                    })
                    .when('/info', {
			templateUrl: 'views/info.html',
			controller: 'InfoCtrl',
			activetab: 'info'
		    }).when('/login', {
			templateUrl: 'views/login.html',
			controller: 'LoginCtrl',
			activetab: 'home'
		    })
                        .otherwise({redirectTo: '/login'});
            }])
        
        .controller('IndexCtrl', ['$scope', '$cookieStore', '$modal', '$window',
	    'SessionService', 'ErrorCMDService', 'UserService',
	    function ($scope, $cookieStore, $modal, $window,
		    SessionService, ErrorCMDService, UserService) {

                $scope.tabHomeState = 'active';
                $scope.tabAdState = '';
                $scope.tabInfoState = '';
                
                $scope.clickHomeTab = function () {
                    console.log('clickHomeTab');
                    $scope.tabHomeState = 'active';
                    $scope.tabAdState = '';
                    $scope.tabInfoState = '';
                    return '#/home';
                };
                
                $scope.clickAdTab = function () {
                    $scope.tabHomeState = '';
                    $scope.tabAdState = 'active';
                    $scope.tabInfoState = '';
                    return '#/advertisements';
                };
                
                $scope.clickInfoTab = function () {
                    $scope.tabHomeState = '';
                    $scope.tabAdState = '';
                    $scope.tabInfoState = 'active';
                    return '#/info';
                };
                
                
		$scope.showLogin = function () {
		    $modal.open({
			templateUrl: 'views/loginmodal.html',
			controller: 'LoginModalCtrl'
		    });
		};
		$scope.isLoggedIn = function () {
		    return SessionService.getUser() !== undefined;
//                    return false;
		};
		$scope.logOut = function () {
		    SessionService.logOut();
		};
		$scope.getProfile = function () {
		    var resp = UserService.getUserBySession({
		    }, function () {
			if (resp.error === 0)
			{
			    $modal.open({
				templateUrl: 'views/userdetail.html',
				controller: 'UserDetailCtrl',
				resolve: {
				    cb: function () {
					return undefined;
				    },
				    user: function () {
					return resp.data;
				    }
				}
			    });
			}
			else
			{
			    //ErrorCMDService.displayError('No Permission');
			}
		    });
		};
		$scope.getUserMenu = function ()
		{
		    return $cookieStore.get('user');
		};
		$scope.userMenu = $scope.getUserMenu();
		$scope.getMenuTabs = function () {

		    var tabs;
		    tabs = [{name: 'home', path: '#/home', title: 'Home'}];
                    tabs.push({name: 'advertisements', path: '#/advertisements', title: 'Advertisements'});
                    tabs.push({name: 'info', path: '#/info', title: 'Info'});

		    return tabs;
		};
		$scope.menutabs = $scope.getMenuTabs();
	    }])
;
'use strict';

angular.module('myApp')
        .controller('MainCtrl', function ($scope, $route, SessionService) {
            $scope.$route = $route;
            
            $scope.getUser = function () {
                var user = SessionService.getUser();
                return user;
            };
            
            $scope.init = function () {
                
            };
            $scope.init();
        });
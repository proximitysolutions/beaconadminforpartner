/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


'use strict';
var app = angular.module('myApp');
app.controller('EditAdvertisementCtrl', ['$scope', '$modalInstance', 'cb',
    'AdvertisementService', 'ErrorCMDService','advertisement',
    function ($scope, $modalInstance, cb,
            AdvertisementService, ErrorCMDService, advertisement) {

        $scope.init = function ()
        {
            $scope.advertisement = advertisement;
            //$scope.newAdvertisement = {title: 'title', content: 'content', startTime: 0, endTime: 1};
            console.debug('****title ***' +$scope.advertisement.title);
            console.debug('***** content *****' + $scope.advertisement.content);
        };
        $scope.init();


        $scope.ok = function () {
	    $modalInstance.close($scope.advertisement);
        };
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        $scope.msgList = [];
        $scope.clearMsg = function () {
            $scope.msgList = [];
        };
        $scope.showMsg = function (msg) {
            $scope.msgList = [{type: 'danger', content: msg}];
        };


        $scope.confirm = function () {
            var resp = AdvertisementService.editAdvertisement({
                title: $scope.advertisement.title,
                content: $scope.advertisement.content,
                startTime: $scope.advertisement.startTime,
                endTime: $scope.advertisement.endTime,
                adId: $scope.advertisement.advertisementId
            }, function () {
                if (resp.error === 0) {
                    $scope.ok();
                    cb();
                }
                else {
                    $scope.showMsg('Add failed');
                }

            });

        }
        ;
    }
]);
'use strict';

/**
 * @ngdoc function
 * @name myAppApp.controller:SubmitmodalCtrl
 * @description
 * # SubmitmodalCtrl
 * Controller of the myAppApp
 */
angular.module('myApp')
	.controller('SubmitModalCtrl', ['$scope', '$modalInstance', 'cb',
	    function ($scope, $modalInstance, cb) {

		$scope.confirm = function () {
		    cb();
		    $scope.ok();
		};

		$scope.ok = function () {
		    $modalInstance.close();
		};
		$scope.cancel = function () {
		    $modalInstance.dismiss('cancel');
		};


	    }]);

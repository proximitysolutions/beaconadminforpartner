/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


'use strict';
var app = angular.module('myApp');
app.controller('BeaconAdvertisCtrl', ['$cookieStore', '$scope', '$route',
    'BeaconService', 'AdvertisementService', 'ErrorCMDService', 'SessionService', 'beaconId','$modalInstance',
    function ($cookieStore, $scope, $route,
            BeaconService, AdvertisementService, ErrorCMDService, SessionService, beaconId ,$modalInstance) {

            
        $scope.listBeaconAdvertis = function (beaconId) {
            console.log('BeaconAdvertisCtrl ' + beaconId);
            var resp = BeaconService.listBeaconAdvertis({
                beaconId: beaconId
            }, function () {
                console.log('listBeaconAdvertis error : ' + resp.error);
                if (resp.error == 0) {
                 $scope.remainAds = resp.data.remainAds;
                 $scope.currentAds = resp.data.currentAds;
                 
                }
                
            });
        };
        $scope.checkBoxChange1 = function (adsId, value) {            
            var action = value?1:0;
            var resp = BeaconService.assignOrRemoveBeaconAdvertis({
                beaconId: beaconId,
                advertisId: adsId,
                action:action
            }, function () {
                console.log('assignOrRemoveBeaconAdvertis error : ' + resp.error);
            });
            
        }
        $scope.remainModel = false;
        $scope.currentModel = true;

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
        
        $scope.init = function () {
            $scope.listBeaconAdvertis(beaconId);
        };
        $scope.init();
    }
]);

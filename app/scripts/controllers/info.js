/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


'use strict';

angular.module('myApp')
        .controller('InfoCtrl', function ($scope, $route, SessionService) {
            $scope.$route = $route;
            $scope.init = function () {
                var user = SessionService.getUser();
                if (user === undefined || user === null) {
                    console.log('chua dang nhap');
                    window.location.replace('#/login');
                    return;
                }
            };
            $scope.init();
        });